# Hand Detection

## Dataset

We generated our own dataset and used some other available datasets to
train and evaluate the model. To download and preprocess the datasets we provide
a script `data_download.py` that can be used.

```bash
python data_download.py
```

## Training

Run the training code:

```bash
python train.py --data_root data/
```

We use Weights & Biases to log the training process.  When you first train, W&B
will prompt you to create a new account and will generate an **API key** for
you. If you are an existing user you can retrieve your key from
https://wandb.ai/authorize. This key is used to tell W&B where to log your data.
You only need to supply your key once, and then it is remembered on the same
device.  W&B will create a cloud project (default is 'COLLABORA') for your training
runs, and each new training run will be provided a unique run name within that
project as project/name.

Run information streams from your environment to the W&B cloud console as you
train. This allows you to monitor and even cancel runs in realtime . All
important information is logged:

- Training & Validation losses
- Metrics: Precision, Recall, mAP@0.5, mAP@0.5:0.95
- Learning Rate over time
- A bounding box debugging panel, showing the training progress over time
- GPU: Type, GPU Utilization, power, temperature, CUDA memory usage
- System: Disk I/0, CPU utilization, RAM memory usage
- Your trained model as W&B Artifact
- Environment: OS and Python types, Git repository and state, training command

## Evaluation

Run the evaluation code:

```bash
python evaluate.py --data_root data/
```

