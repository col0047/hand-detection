#include <string.h>
#include <stdio.h>
#include <glib.h>
#include <gst/gst.h>
#include <glib/gstdio.h>
#include <assert.h>
#include <regex>
#include <sstream>

/*

To play stream #4 from the mastroska file:

$ gst-launch-1.0 playbin3 current_video=4 uri=file:///home/aaron/temp/capture.mka

 */


/*
  exposure            : Exposure time (µs)
                        flags: readable, writable
                        Double. Range:              -1 -           1e+08 Default:              -1
  exposure-auto       : Auto Exposure Mode
                        flags: readable, writable
                        Enum "GstArvAuto" Default: 0, "off"
                           (0): off              - Off
                           (1): once             - Once
                           (2): on               - Continuous

  gain                : Gain (dB)
                        flags: readable, writable
                        Double. Range:              -1 -             500 Default:              -1
  gain-auto           : Auto Gain Mode
                        flags: readable, writable
                        Enum "GstArvAuto" Default: 0, "off"
                           (0): off              - Off
                           (1): once             - Once
                           (2): on               - Continuous
 */

static const int max_sources = 64;
static const std::regex INT_TYPE("[+-]?[0-9]+");

enum CameraMode{
	GAIN,GAIN_AUTO,EXPOSURE,EXPOSURE_AUTO
};

typedef struct _CustomData
{
  GstElement *pipeline;
  GstElement *src[max_sources];
  gint num_sources;
  CameraMode mode;
  GMainLoop *loop;

  gboolean playing;             /* Playing or Paused */
} CustomData;


static std::string getModeProp(CustomData * data){
  std::string prop = "";
  switch(data->mode){
  case GAIN:
	  prop = "gain";
	  break;
  case GAIN_AUTO:
	  prop = "gain-auto";
	  break;
  case EXPOSURE:
	  prop = "exposure";
	  break;
  case EXPOSURE_AUTO:
	  prop = "exposure-auto";
	  break;
  default:
	  break;
  }
  return prop;
}


/* Process keyboard input */
static gboolean
handle_keyboard (GIOChannel * source, GIOCondition cond, CustomData * data)
{
  gchar *line = NULL;

  if (g_io_channel_read_line (source, &line, NULL, NULL,
          NULL) != G_IO_STATUS_NORMAL) {
    return TRUE;
  }

  std::string prop = getModeProp(data);
  std::string str = line;
  str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());
  if (std::regex_match(str, INT_TYPE)){
	  std::stringstream ss(str);
	  int val;
	  ss >> val;
	  for (gint i = 0; i < data->num_sources; ++i){
		  g_object_set(data->src[i], prop.c_str(), val, NULL);
	  }
  } else {
	  gint val = -1;
	  switch (g_ascii_tolower (line[0])) {
	    case 'd':
	    	gst_debug_bin_to_dot_file_with_ts(GST_BIN(data->pipeline),
	    			GST_DEBUG_GRAPH_SHOW_ALL, "pipeline");
	    	break;
		case 'p':
		  data->playing = !data->playing;
		  gst_element_set_state (data->pipeline,
			  data->playing ? GST_STATE_PLAYING : GST_STATE_PAUSED);
		  g_print ("Setting state to %s\n", data->playing ? "PLAYING" : "PAUSE");
		  break;
		case 'q':
		  g_main_loop_quit (data->loop);
		  break;
		case 'g':
		  data->mode = GAIN;
		  g_print ("Setting mode to gain\n");
		  g_object_get(data->src[0], prop.c_str(), &val, NULL);
		  break;
		case 'h':
		  data->mode = GAIN_AUTO;
		  g_print ("Setting mode to gain-auto\n");
		  g_object_get(data->src[0], prop.c_str(), &val, NULL);
		  break;
		case 'e':
		  data->mode = EXPOSURE;
		  g_print ("Setting mode to exposure\n");
		  g_object_get(data->src[0], prop.c_str(), &val, NULL);
		  break;
		case 'f':
		  data->mode = EXPOSURE_AUTO;
		  g_print ("Setting mode to exposure-auto\n");
		  g_object_get(data->src[0], prop.c_str(), &val, NULL);
		  break;
		default:
		  break;
	  }
	  if (val != -1)
		  g_print ("Current value is %d\n", val);
  }

  g_free (line);

  return TRUE;
}

int
main (int argc, char *argv[])
{
  CustomData data;
  GstStateChangeReturn ret;
  GIOChannel *io_stdin = nullptr;

  //for (int i = 0; i < argc; ++i)
	//  g_print("command line: %s\n",argv[i]);

  /* Initialize GStreamer */
  if (argc > 3)
	  gst_debug_set_threshold_from_string(argv[3], TRUE);
  std::string fname;
  if (argc > 2)
	  fname = argv[2];
  g_setenv("GST_DEBUG_DUMP_DOT_DIR", "/tmp", TRUE);
  gst_init (&argc, &argv);

  /* Initialize our data structure */
  memset (&data, 0, sizeof (data));

  /* Print usage map */
  g_print ("USAGE: Choose one of the following options, then press enter:\n"
      " 'p' to toggle between PAUSE and PLAY\n"
	  " 'd' to capture pipeline diagram\n"
	  " 'q' to quit\n"
	  " 'g' to enter gain mode\n"
	  " 'h' to enter gain-auto mode\n"
	  " 'e' to enter exposure mode\n"
	  " 'f' to enter exposure-auto mode\n");

  // default pipeline
  #define LINE_SIZE 100

  char * pipe = new char[LINE_SIZE * 100];
  // read configuration file, if present
  FILE* fp = fopen(fname.c_str(), "r");
  char* line = NULL;
  if (fp ) {
	  pipe[0] = 0;
	  size_t len = 0;
	  while ((getline(&line, &len, fp)) != -1) {
		  if (line != nullptr && line[0] != '#')
			  strcat(pipe, line);
	  }
	  fclose(fp);
  } else {
	  g_print("Unable to open file %s", fname.c_str());
	  return 1;
  }

  // read from command line
  /* Build the pipeline */
  data.pipeline =
      gst_parse_launch
      (pipe,
      NULL);

  for (gint i = 0; i < max_sources; ++i){
	  std::stringstream ss;
	  ss << "src" << i;
	  auto temp = gst_bin_get_by_name(GST_BIN(data.pipeline), ss.str().c_str());
	  if (temp) {
		  data.src[i] = temp;
		  data.num_sources++;
	  } else {
		  break;
	  }
  }

  /* Add a keyboard watch so we get notified of keystrokes */
#ifdef G_OS_WIN32
  io_stdin = g_io_channel_win32_new_fd (fileno (stdin));
#else
  io_stdin = g_io_channel_unix_new (fileno (stdin));
#endif
  g_io_add_watch (io_stdin, G_IO_IN, (GIOFunc) handle_keyboard, &data);

  /* Start playing */
  ret = gst_element_set_state (data.pipeline, GST_STATE_PLAYING);
  if (ret == GST_STATE_CHANGE_FAILURE) {
    g_printerr ("Unable to set the pipeline to the playing state.\n");
    gst_object_unref (data.pipeline);
    return -1;
  }
  data.playing = TRUE;

  /* Create a GLib Main Loop and set it to run */
  data.loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (data.loop);

  /* Free resources */
  g_print ("Shutting down ...\n");
  g_main_loop_unref (data.loop);
  g_io_channel_unref (io_stdin);
  gst_element_set_state (data.pipeline, GST_STATE_NULL);
  for (gint i = 0; i < max_sources; ++i){
	  if (data.src[i])
		  gst_object_unref(data.src[i]);
  }
  gst_object_unref (data.pipeline);
  free(line);
  delete[] pipe;
  g_print ("Shutdown complete\n");
  return 0;
}
